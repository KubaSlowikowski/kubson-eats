package pl.kubson_eats.utils;

public enum ProductCategory {
    APPETIZER,
    BREAKFAST,
    MAIN_DISH,
    DESSERT,
    SNACK,
    SOUP,
    DRINK,
    ALCOHOL
}

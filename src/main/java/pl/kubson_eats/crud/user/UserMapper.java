package pl.kubson_eats.crud.user;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import pl.kubson_eats.crud.abstraction.CommonMapper;
import pl.kubson_eats.crud.address.AddressMapper;

@Mapper(componentModel = "spring", uses = {AddressMapper.class})
public interface UserMapper extends CommonMapper<User, UserDTO> {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Override
    @Mapping(source = "encodedPassword", target = "password")
    UserDTO toDto(User user);

    @Override
    @Mapping(source = "password", target = "encodedPassword")
    User fromDto(UserDTO dto);
}

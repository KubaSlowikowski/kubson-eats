package pl.kubson_eats.crud.image;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kubson_eats.crud.abstraction.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "images")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Image extends AbstractEntity {

    private String fileUrl;

    private String title;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Image image = (Image) o;
        return Objects.equals(fileUrl, image.fileUrl) &&
                Objects.equals(getId(), image.getId()) &&
                Objects.equals(title, image.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

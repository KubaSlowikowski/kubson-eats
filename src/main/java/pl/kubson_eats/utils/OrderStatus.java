package pl.kubson_eats.utils;

public enum OrderStatus {
    IN_DELIVERY,
    READY,
    IN_PROGRESS,
    WAITING,
    CLOSED
}

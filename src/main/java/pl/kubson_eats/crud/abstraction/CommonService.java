package pl.kubson_eats.crud.abstraction;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CommonService<D extends AbstractDto> {
    Page<D> getAll(Pageable pageable, String search);

    D findById(Long id);

    D save(D dto);

    D update(Long id, D dto);

    D delete(Long id);

}

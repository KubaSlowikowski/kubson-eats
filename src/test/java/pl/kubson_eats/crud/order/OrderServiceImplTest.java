package pl.kubson_eats.crud.order;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import pl.kubson_eats.crud.abstraction.AbstractService;
import pl.kubson_eats.crud.abstraction.AbstractServiceTest;
import pl.kubson_eats.crud.abstraction.CommonMapper;
import pl.kubson_eats.crud.abstraction.CommonRepository;

public class OrderServiceImplTest extends AbstractServiceTest<Order, OrderDTO> {

    @InjectMocks
    private OrderServiceImpl orderService;
    @Mock
    private OrderRepository orderRepository;
    @Mock
    private OrderMapper orderMapper;



    @Override
    public AbstractService<Order, OrderDTO> getService() {
        return orderService;
    }

    @Override
    public CommonRepository<Order> getRepostiory() {
        return orderRepository;
    }

    @Override
    public CommonMapper<Order, OrderDTO> getMapper() {
        return orderMapper;
    }

    @Override
    public Class<OrderDTO> getDtoClass() {
        return OrderDTO.class;
    }

    @Override
    public Class<Order> getEntityClass() {
        return Order.class;
    }
}
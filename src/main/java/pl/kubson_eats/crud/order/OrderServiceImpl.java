package pl.kubson_eats.crud.order;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.kubson_eats.crud.abstraction.AbstractService;
import pl.kubson_eats.crud.exception.NotFoundException;
import pl.kubson_eats.crud.exception.SameStatusException;
import pl.kubson_eats.crud.product.Product;
import pl.kubson_eats.crud.product.ProductRepository;
import pl.kubson_eats.utils.OrderStatus;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl extends AbstractService<Order, OrderDTO> implements OrderService {

    private final OrderMapper orderMapper;
    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;

    protected OrderServiceImpl(final OrderMapper orderMapper, final OrderRepository orderRepository, final ProductRepository productRepository) {
        super(orderMapper, orderRepository);
        this.orderMapper = orderMapper;
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }

    @Override
    public OrderDTO save(final OrderDTO dto) {
        if (dto != null && dto.getOrderStatus() == null) {
            dto.setOrderStatus(OrderStatus.WAITING);
        }
        return super.save(dto);
    }

    @Override
    public Page<OrderDTO> findAllByRestaurantId(final Long restaurantId, final Pageable pageable) {
        List<Product> products = productRepository.findAllByRestaurantId(restaurantId, Pageable.unpaged());
        List<Order> results = orderRepository.findAllByProductsIn(products, pageable).getContent().stream().distinct().collect(Collectors.toList());
        return new PageImpl<>(orderMapper.toListDto(results), pageable, results.size());
    }

    @Override
    public Page<OrderDTO> findAllbyRestauratnIdAndOrderStatus(final Long restaurantId, final OrderStatus status) {
        List<Product> products = productRepository.findAllByRestaurantId(restaurantId, Pageable.unpaged());
        List<Order> results = orderRepository.findAllByProductsInAndOrderStatus(products, status, Pageable.unpaged()).getContent().stream().distinct().collect(Collectors.toList());
        return new PageImpl<>(orderMapper.toListDto(results), Pageable.unpaged(), results.size());
    }

    @Override
    public Page<OrderDTO> findAllByUserId(final Long userId, final Pageable pageable) {
        List<Order> results = orderRepository.findAllByUserId(userId, pageable).stream().distinct().collect(Collectors.toList());
        return new PageImpl<>(orderMapper.toListDto(results), pageable, results.size());
    }

    @Override
    public Page<OrderDTO> findAllByOrderStatus(OrderStatus status, Pageable pageable) {
        List<Order> orders = orderRepository.findAllByOrderStatus(status, pageable);
        return new PageImpl<>(orderMapper.toListDto(orders), pageable, orders.size());
    }

    @Override
    public Page<OrderDTO> findOrdersByUserIdAndStatus(Long userId, OrderStatus status, Pageable pageable) {
        orderRepository.findById(userId).orElseThrow(() -> new NotFoundException(userId, Order.class.getSimpleName()));
        List<Order> orders = orderRepository.findAllByUserIdAndOrderStatus(userId, status, pageable);
        return new PageImpl<>(orderMapper.toListDto(orders), pageable, orders.size());
    }

    @Override
    public void changeStatus(Long id, OrderStatus status) {
        Order order = orderRepository.findById(id).orElseThrow(() -> new NotFoundException(id, Order.class.getSimpleName()));
        if (order.getOrderStatus() == status) {
            throw new SameStatusException(id, status);
        } else {
            order.setOrderStatus(status);
            orderMapper.toDto(orderRepository.save(order));
        }
    }
}

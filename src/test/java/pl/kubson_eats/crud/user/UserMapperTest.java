package pl.kubson_eats.crud.user;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import pl.kubson_eats.crud.address.AddressMapper;
import pl.kubson_eats.utils.Utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@RunWith(MockitoJUnitRunner.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserMapperTest {

    @InjectMocks
    UserMapper userMapper = UserMapperImpl.INSTANCE;

    @BeforeAll
    void init() {
        AddressMapper addressMapper = Mappers.getMapper(AddressMapper.class);
        ReflectionTestUtils.setField(userMapper, "addressMapper", addressMapper);
    }

    @Test
    void should_map_user_to_userDto() {
        //given
        User user = Utils.getUser();
        UserDTO userDTO = Utils.getUserDTO();

        //when
        UserDTO result = userMapper.toDto(user);

        //then
        assertEquals(userDTO, result);
    }

    @Test
    void shouldnt_map_user_to_userDto() {
        //given
        User user = Utils.getUser();
        UserDTO userDTO = Utils.getUserDTO();
        userDTO.setName("123123c12d24");

        //when
        UserDTO result = userMapper.toDto(user);

        //then
        assertNotEquals(userDTO, result);
    }

    @Test
    void should_map_userDto_to_user() {
        //given
        User user = Utils.getUser();
        UserDTO userDTO = Utils.getUserDTO();

        //when
        User result = userMapper.fromDto(userDTO);

        //then
        assertEquals(user, result);
    }

    @Test
    void shouldnt_map_userDto_to_user() {
        //given
        User user = Utils.getUser();
        UserDTO userDTO = Utils.getUserDTO();
        userDTO.setName("1290378fh09");

        //when
        User result = userMapper.fromDto(userDTO);

        //then
        assertNotEquals(user, result);
    }
}

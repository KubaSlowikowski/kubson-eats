package pl.kubson_eats.crud.image;

import org.mapstruct.Mapper;
import pl.kubson_eats.crud.abstraction.CommonMapper;

@Mapper(componentModel = "spring")
public interface ImageMapper extends CommonMapper<Image, ImageDTO> {
}

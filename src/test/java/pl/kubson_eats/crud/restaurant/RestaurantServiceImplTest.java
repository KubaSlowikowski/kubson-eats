package pl.kubson_eats.crud.restaurant;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import pl.kubson_eats.crud.abstraction.AbstractService;
import pl.kubson_eats.crud.abstraction.AbstractServiceTest;
import pl.kubson_eats.crud.abstraction.CommonMapper;
import pl.kubson_eats.crud.abstraction.CommonRepository;

import static org.junit.jupiter.api.Assertions.*;


public class RestaurantServiceImplTest extends AbstractServiceTest<Restaurant, RestaurantDTO> {

    @InjectMocks
    private RestaurantServiceImpl restaurantService;
    @Mock
    private RestaurantRepository restaurantRepository;
    @Mock
    private RestaurantMapper restaurantMapper;

    @Override
    public AbstractService<Restaurant, RestaurantDTO> getService() {
        return restaurantService;
    }

    @Override
    public CommonRepository<Restaurant> getRepostiory() {
        return restaurantRepository;
    }

    @Override
    public CommonMapper<Restaurant, RestaurantDTO> getMapper() {
        return restaurantMapper;
    }

    @Override
    public Class<RestaurantDTO> getDtoClass() {
        return RestaurantDTO.class;
    }

    @Override
    public Class<Restaurant> getEntityClass() {
        return Restaurant.class;
    }
}
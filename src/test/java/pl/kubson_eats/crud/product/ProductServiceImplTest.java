package pl.kubson_eats.crud.product;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import pl.kubson_eats.crud.abstraction.AbstractService;
import pl.kubson_eats.crud.abstraction.AbstractServiceTest;
import pl.kubson_eats.crud.abstraction.CommonMapper;
import pl.kubson_eats.crud.abstraction.CommonRepository;

import static org.junit.jupiter.api.Assertions.*;

public class ProductServiceImplTest extends AbstractServiceTest<Product, ProductDTO> {

    @InjectMocks
    private ProductServiceImpl productService;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private ProductMapper productMapper;

    @Override
    public AbstractService<Product, ProductDTO> getService() {
        return productService;
    }

    @Override
    public CommonRepository<Product> getRepostiory() {
        return productRepository;
    }

    @Override
    public CommonMapper<Product, ProductDTO> getMapper() {
        return productMapper;
    }

    @Override
    public Class<ProductDTO> getDtoClass() {
        return ProductDTO.class;
    }

    @Override
    public Class<Product> getEntityClass() {
        return Product.class;
    }
}
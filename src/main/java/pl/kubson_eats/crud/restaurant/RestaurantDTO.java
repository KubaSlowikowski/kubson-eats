package pl.kubson_eats.crud.restaurant;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kubson_eats.crud.abstraction.AbstractDto;
import pl.kubson_eats.crud.address.AddressDTO;
import pl.kubson_eats.crud.product.ProductDTO;
import pl.kubson_eats.utils.RestaurantCategory;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.PositiveOrZero;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class RestaurantDTO extends AbstractDto {

    @NotBlank(message = "Restaurant's name must not be null or empty")
    private String name;

    @NotBlank(message = "Restaurant's description must not be null or empty")
    private String description;

    private Set<ProductDTO> products;

    @Valid
    private AddressDTO address;

    @Pattern(regexp = "[0-9]{9}", message = "Restaurant's phone number must not be null and be formatted properly")
    private String phoneNumber;

    @NotBlank(message = "Restaurant's email must not be null or empty")
    @Email
    private String email;

    @NotBlank(message = "Restaurant's login must not be null or empty")
    private String login;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotBlank(message = "Restaurant's password must not be null or empty")
    private String password;

    private RestaurantCategory category;

    @PositiveOrZero
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long imageId;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof RestaurantDTO)) return false;
        RestaurantDTO that = (RestaurantDTO) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(products, that.products) &&
                Objects.equals(address, that.address) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(email, that.email) &&
                Objects.equals(login, that.login) &&
                Objects.equals(password, that.password) &&
                Objects.equals(imageId, that.imageId) &&
                category == that.category;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public static final class RestaurantDTOBuilder {
        private Long id;
        private String name;
        private String description;
        private Set<ProductDTO> products;
        private AddressDTO address;
        private String phoneNumber;
        private String email;
        private String login;
        private String password;
        private RestaurantCategory category;
        private Long imageId;

        private RestaurantDTOBuilder() {
        }

        public static RestaurantDTOBuilder aRestaurantDTO() {
            return new RestaurantDTOBuilder();
        }

        public RestaurantDTOBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public RestaurantDTOBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public RestaurantDTOBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public RestaurantDTOBuilder withProducts(Set<ProductDTO> products) {
            this.products = products;
            return this;
        }

        public RestaurantDTOBuilder withAddress(AddressDTO address) {
            this.address = address;
            return this;
        }

        public RestaurantDTOBuilder withPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public RestaurantDTOBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public RestaurantDTOBuilder withLogin(String login) {
            this.login = login;
            return this;
        }

        public RestaurantDTOBuilder withPassword(String password) {
            this.password = password;
            return this;
        }

        public RestaurantDTOBuilder withCategory(RestaurantCategory category) {
            this.category = category;
            return this;
        }

        public RestaurantDTOBuilder withImageId(Long imageId) {
            this.imageId = imageId;
            return this;
        }

        public RestaurantDTO build() {
            RestaurantDTO restaurantDTO = new RestaurantDTO();
            restaurantDTO.setId(id);
            restaurantDTO.setName(name);
            restaurantDTO.setDescription(description);
            restaurantDTO.setProducts(products);
            restaurantDTO.setAddress(address);
            restaurantDTO.setPhoneNumber(phoneNumber);
            restaurantDTO.setEmail(email);
            restaurantDTO.setLogin(login);
            restaurantDTO.setPassword(password);
            restaurantDTO.setCategory(category);
            restaurantDTO.setImageId(imageId);
            return restaurantDTO;
        }
    }
}

package pl.kubson_eats.crud.user;

import org.springframework.stereotype.Service;
import pl.kubson_eats.crud.abstraction.AbstractService;

@Service
public class UserServiceImpl extends AbstractService<User, UserDTO> implements UserService {

    protected UserServiceImpl(final UserMapper userMapper, final UserRepository userRepository) {
        super(userMapper, userRepository);
    }
}

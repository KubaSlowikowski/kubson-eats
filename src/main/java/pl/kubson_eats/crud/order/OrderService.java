package pl.kubson_eats.crud.order;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.kubson_eats.crud.abstraction.CommonService;
import pl.kubson_eats.utils.OrderStatus;

@Service
public interface OrderService extends CommonService<OrderDTO> {
    Page<OrderDTO> findAllByRestaurantId(Long restaurantId, Pageable pageable);

    Page<OrderDTO> findAllbyRestauratnIdAndOrderStatus(Long restaurantId, OrderStatus status);

    Page<OrderDTO> findAllByUserId(Long userId, Pageable page);

    Page<OrderDTO> findAllByOrderStatus(OrderStatus status, Pageable pageable);

    Page<OrderDTO> findOrdersByUserIdAndStatus(Long userId, OrderStatus status, Pageable pageable);

    void changeStatus(Long id, OrderStatus status);
}

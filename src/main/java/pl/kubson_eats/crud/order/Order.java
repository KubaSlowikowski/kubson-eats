package pl.kubson_eats.crud.order;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kubson_eats.crud.abstraction.AbstractEntity;
import pl.kubson_eats.crud.product.Product;
import pl.kubson_eats.crud.user.User;
import pl.kubson_eats.utils.OrderStatus;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "orders")
@NoArgsConstructor
@Getter
@Setter
public class Order extends AbstractEntity {

    private String orderComment;

    @ManyToOne(fetch = FetchType.LAZY)
    //FIXME cannot DELETE user when having orders. 'update or delete on table "users" violates foreign key constraint'
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToMany
    @JoinTable(
            name = "order_products",
            joinColumns = @JoinColumn(name = "order_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id")
    )
    private Set<Product> products;

    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return Objects.equals(getId(), order.getId()) &&
                Objects.equals(orderComment, order.orderComment) &&
                Objects.equals(user, order.user) &&
                Objects.equals(getCreatedOn(), order.getCreatedOn()) &&
                Objects.equals(products, order.products) &&
                orderStatus == order.orderStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public static final class OrderBuilder {
        private String orderComment;
        private User user;
        private LocalDateTime createdOn;
        private Set<Product> products;
        private OrderStatus orderStatus;
        private Long id;

        private OrderBuilder() {
        }

        public static OrderBuilder anOrder() {
            return new OrderBuilder();
        }

        public OrderBuilder withOrderComment(String orderComment) {
            this.orderComment = orderComment;
            return this;
        }

        public OrderBuilder withUser(User user) {
            this.user = user;
            return this;
        }

        public OrderBuilder withCreatedOn(LocalDateTime createdOn) {
            this.createdOn = createdOn;
            return this;
        }

        public OrderBuilder withProducts(Set<Product> products) {
            this.products = products;
            return this;
        }

        public OrderBuilder withOrderStatus(OrderStatus orderStatus) {
            this.orderStatus = orderStatus;
            return this;
        }

        public OrderBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public Order build() {
            Order order = new Order();
            order.setOrderComment(orderComment);
            order.setUser(user);
            order.setCreatedOn(createdOn);
            order.setProducts(products);
            order.setOrderStatus(orderStatus);
            order.setId(id);
            return order;
        }
    }
}

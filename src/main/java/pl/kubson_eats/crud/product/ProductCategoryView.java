package pl.kubson_eats.crud.product;

import pl.kubson_eats.utils.ProductCategory;

import java.util.Objects;

public class ProductCategoryView { //projection
    private ProductCategory category;

    public ProductCategoryView(ProductCategory category) {
        this.category = category;
    }

    public ProductCategory getCategory() {
        return category;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final ProductCategoryView that = (ProductCategoryView) o;
        return category == that.category;
    }

    @Override
    public int hashCode() {
        return Objects.hash(category);
    }
}

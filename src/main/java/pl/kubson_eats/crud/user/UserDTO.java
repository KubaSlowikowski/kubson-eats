package pl.kubson_eats.crud.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kubson_eats.crud.abstraction.AbstractDto;
import pl.kubson_eats.crud.address.AddressDTO;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO extends AbstractDto {

    @NotBlank(message = "User's name must not be null or empty")
    private String name;

    @NotBlank(message = "User's second name must not be null or empty")
    private String secondName;

    @NotBlank(message = "User's email must not be null or empty")
    @Email
    private String email;

    @Valid
    private AddressDTO address;

    @Pattern(regexp = "[0-9]{9}", message = "User's phone number must not be null and be formatted properly")
    private String phoneNumber;

    @NotBlank(message = "User's login must not be null or empty")
    private String login;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotBlank(message = "User's password must not be null or empty")
    private String password;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final UserDTO userDTO = (UserDTO) o;
        return Objects.equals(getId(), userDTO.getId()) &&
                Objects.equals(name, userDTO.name) &&
                Objects.equals(secondName, userDTO.secondName) &&
                Objects.equals(email, userDTO.email) &&
                Objects.equals(address, userDTO.address) &&
                Objects.equals(phoneNumber, userDTO.phoneNumber) &&
                Objects.equals(login, userDTO.login) &&
                Objects.equals(password, userDTO.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public static final class UserDTOBuilder {
        private Long id;
        private String name;
        private String secondName;
        private String email;
        private AddressDTO address;
        private String phoneNumber;
        private String login;
        private String password;

        private UserDTOBuilder() {
        }

        public static UserDTOBuilder anUserDTO() {
            return new UserDTOBuilder();
        }

        public UserDTOBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public UserDTOBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public UserDTOBuilder withSecondName(String secondName) {
            this.secondName = secondName;
            return this;
        }

        public UserDTOBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public UserDTOBuilder withAddress(AddressDTO address) {
            this.address = address;
            return this;
        }

        public UserDTOBuilder withPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public UserDTOBuilder withLogin(String login) {
            this.login = login;
            return this;
        }

        public UserDTOBuilder withPassword(String password) {
            this.password = password;
            return this;
        }

        public UserDTO build() {
            UserDTO userDTO = new UserDTO();
            userDTO.setId(id);
            userDTO.setName(name);
            userDTO.setSecondName(secondName);
            userDTO.setEmail(email);
            userDTO.setAddress(address);
            userDTO.setPhoneNumber(phoneNumber);
            userDTO.setLogin(login);
            userDTO.setPassword(password);
            return userDTO;
        }
    }
}

package pl.kubson_eats.crud.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kubson_eats.crud.abstraction.AbstractEntity;
import pl.kubson_eats.crud.address.Address;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "users",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "email"),
                @UniqueConstraint(columnNames = "login"),
                @UniqueConstraint(columnNames = "phoneNumber")
        })
@NoArgsConstructor
@Getter
@Setter
public class User extends AbstractEntity {

    private String name;

    private String secondName;

    private String email;

    private String login;

    private String encodedPassword;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;

    private String phoneNumber;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(getId(), user.getId()) &&
                Objects.equals(name, user.name) &&
                Objects.equals(secondName, user.secondName) &&
                Objects.equals(email, user.email) &&
                Objects.equals(login, user.login) &&
                Objects.equals(encodedPassword, user.encodedPassword) &&
                Objects.equals(address, user.address) &&
                Objects.equals(phoneNumber, user.phoneNumber) &&
                Objects.equals(getCreatedOn(), user.getCreatedOn());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public static final class UserBuilder {
        private String name;
        private String secondName;
        private String email;
        private String login;
        private String encodedPassword;
        private Address address;
        private String phoneNumber;
        private Long id;
        private LocalDateTime createdOn;

        private UserBuilder() {
        }

        public static UserBuilder anUser() {
            return new UserBuilder();
        }

        public UserBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public UserBuilder withSecondName(String secondName) {
            this.secondName = secondName;
            return this;
        }

        public UserBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public UserBuilder withLogin(String login) {
            this.login = login;
            return this;
        }

        public UserBuilder withEncodedPassword(String encodedPassword) {
            this.encodedPassword = encodedPassword;
            return this;
        }

        public UserBuilder withAddress(Address address) {
            this.address = address;
            return this;
        }

        public UserBuilder withPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public UserBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public UserBuilder withCreatedOn(LocalDateTime createdOn) {
            this.createdOn = createdOn;
            return this;
        }

        public User build() {
            User user = new User();
            user.setName(name);
            user.setSecondName(secondName);
            user.setEmail(email);
            user.setLogin(login);
            user.setEncodedPassword(encodedPassword);
            user.setAddress(address);
            user.setPhoneNumber(phoneNumber);
            user.setId(id);
            user.setCreatedOn(createdOn);
            return user;
        }
    }
}

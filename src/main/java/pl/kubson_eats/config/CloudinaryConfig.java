package pl.kubson_eats.config;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.kubson_eats.crud.image.ImageUploader;

@Configuration
class CloudinaryConfig {

    @Value("${kubson-eats.cloudinary.cloud-name}")
    private String cloudName;

    @Value("${kubson-eats.cloudinary.api_key}")
    private String apiKey;

    @Value("${kubson-eats.cloudinary.api_secret}")
    private String apiSecret;

    @Bean
    Cloudinary cloudinary() {
        return new Cloudinary(ObjectUtils.asMap(
                "cloud_name", cloudName,
                "api_key", apiKey,
                "api_secret", apiSecret
        ));
    }

    @Bean
    ImageUploader imageUploader(Cloudinary cloudinary) {
        return new ImageUploader(cloudinary);
    }
}

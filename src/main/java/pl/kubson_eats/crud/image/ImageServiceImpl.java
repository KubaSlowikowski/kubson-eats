package pl.kubson_eats.crud.image;

import com.cloudinary.utils.ObjectUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.kubson_eats.crud.exception.NotFoundException;

import java.util.Map;

@Service
@Slf4j
public class ImageServiceImpl implements ImageService {

    private final ImageRepository imageRepository;
    private final ImageUploader imageUploader;
    private final ImageMapper imageMapper;

    protected ImageServiceImpl(final ImageRepository imageRepository, final ImageUploader imageUploader, final ImageMapper imageMapper) {
        this.imageRepository = imageRepository;
        this.imageUploader = imageUploader;
        this.imageMapper = imageMapper;
    }

    @Override
    @SneakyThrows
    public ImageDTO createNewImage(final ImageToUpload imageToUpload) {
        Map uploadResult = imageUploader.uploadImage(imageToUpload.getFile().getBytes());
        imageToUpload.setParams(uploadResult);
        Image image =  imageRepository.save(
                new Image(uploadResult.get("url").toString(), imageToUpload.getPreloadedFile())
        );

        log.info("Image: " + image.getTitle() + " saved and uploaded successfully.");
        return imageMapper.toDto(image);
    }

    @Override
    public ImageDTO findById(final Long id) {
        Image image = getImageById(id);
        return imageMapper.toDto(image);
    }

    @Override
    public void delete(final Long id) {
        Image image = getImageById(id);
        String publicId = image.getTitle().split("/")[3].split("\\.")[0];
        Map response = imageUploader.deleteImage(publicId, ObjectUtils.emptyMap());
        if(!response.get("result").equals("ok")) {
            log.error("Cloudinary API exception occurred when trying to delete an image with id=" + image.getId() + ". Result: '" + response.get("result") + "'");
        }
    }


    private Image getImageById(final Long id) {
        return imageRepository.findById(id).orElseThrow(() -> new NotFoundException(id, "Image"));
    }

}

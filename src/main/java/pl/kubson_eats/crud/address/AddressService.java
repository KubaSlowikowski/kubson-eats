package pl.kubson_eats.crud.address;

import org.springframework.stereotype.Service;
import pl.kubson_eats.crud.abstraction.CommonService;

@Service
public interface AddressService extends CommonService<AddressDTO> {
}

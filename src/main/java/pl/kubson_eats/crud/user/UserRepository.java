package pl.kubson_eats.crud.user;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;
import pl.kubson_eats.crud.abstraction.CommonRepository;

@Repository
public interface UserRepository extends CommonRepository<User> {

    @Override
    @EntityGraph(attributePaths = {"address"})
    Page<User> findAll(Specification<User> specification, Pageable pageable);
}

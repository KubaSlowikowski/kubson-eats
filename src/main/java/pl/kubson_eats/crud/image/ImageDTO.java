package pl.kubson_eats.crud.image;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kubson_eats.crud.abstraction.AbstractDto;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class ImageDTO extends AbstractDto {

    private String fileUrl;

    private String title;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final ImageDTO imageDTO = (ImageDTO) o;
        return Objects.equals(fileUrl, imageDTO.fileUrl) &&
                Objects.equals(title, imageDTO.title) &&
                Objects.equals(getId(), imageDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

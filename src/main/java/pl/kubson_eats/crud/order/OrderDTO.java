package pl.kubson_eats.crud.order;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kubson_eats.crud.abstraction.AbstractDto;
import pl.kubson_eats.crud.product.ProductDTO;
import pl.kubson_eats.utils.OrderStatus;

import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class OrderDTO extends AbstractDto {

    private String orderComment;

    private Long userId;

    //@NotNull(message = "Cannot create an order without any products") //TODO dodac potem
    private Set<ProductDTO> products;

    private OrderStatus orderStatus;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderDTO)) return false;
        OrderDTO orderDTO = (OrderDTO) o;
        return Objects.equals(getId(), orderDTO.getId()) &&
                Objects.equals(orderComment, orderDTO.orderComment) &&
                Objects.equals(userId, orderDTO.userId) &&
                Objects.equals(products, orderDTO.products) &&
                orderStatus == orderDTO.orderStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public static final class OrderDTOBuilder {
        private String orderComment;
        private Long userId;
        private Set<ProductDTO> products;
        private OrderStatus orderStatus;
        private Long id;

        private OrderDTOBuilder() {
        }

        public static OrderDTOBuilder anOrderDTO() {
            return new OrderDTOBuilder();
        }

        public OrderDTOBuilder withOrderComment(String orderComment) {
            this.orderComment = orderComment;
            return this;
        }

        public OrderDTOBuilder withUserId(Long userId) {
            this.userId = userId;
            return this;
        }

        public OrderDTOBuilder withProducts(Set<ProductDTO> products) {
            this.products = products;
            return this;
        }

        public OrderDTOBuilder withOrderStatus(OrderStatus orderStatus) {
            this.orderStatus = orderStatus;
            return this;
        }

        public OrderDTOBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public OrderDTO build() {
            OrderDTO orderDTO = new OrderDTO();
            orderDTO.setOrderComment(orderComment);
            orderDTO.setUserId(userId);
            orderDTO.setProducts(products);
            orderDTO.setOrderStatus(orderStatus);
            orderDTO.setId(id);
            return orderDTO;
        }
    }
}

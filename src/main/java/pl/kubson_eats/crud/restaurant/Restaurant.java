package pl.kubson_eats.crud.restaurant;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kubson_eats.crud.abstraction.AbstractEntity;
import pl.kubson_eats.crud.address.Address;
import pl.kubson_eats.crud.image.Image;
import pl.kubson_eats.crud.product.Product;
import pl.kubson_eats.utils.RestaurantCategory;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "restaurants",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "login")
        })
@NoArgsConstructor
@Getter
@Setter
public class Restaurant extends AbstractEntity {

    private String name;

    private String description;

    @OneToMany(mappedBy = "restaurant", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private Set<Product> products;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;

    private String phoneNumber;

    private String email;

    private String login;

    private String encodedPassword;

    @Enumerated(EnumType.STRING)
    private RestaurantCategory category;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "image_id", referencedColumnName = "id")
    private Image image;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Restaurant)) return false;
        Restaurant that = (Restaurant) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(products, that.products) &&
                Objects.equals(address, that.address) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(email, that.email) &&
                Objects.equals(login, that.login) &&
                Objects.equals(encodedPassword, that.encodedPassword) &&
                Objects.equals(image, that.image) &&
                category == that.category;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public static final class RestaurantBuilder {
        private Long id;
        private LocalDateTime createdOn;
        private String name;
        private String description;
        private Set<Product> products;
        private Address address;
        private String phoneNumber;
        private String email;
        private String login;
        private String encodedPassword;
        private RestaurantCategory category;
        private Image image;

        private RestaurantBuilder() {
        }

        public static RestaurantBuilder aRestaurant() {
            return new RestaurantBuilder();
        }

        public RestaurantBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public RestaurantBuilder withCreatedOn(LocalDateTime createdOn) {
            this.createdOn = createdOn;
            return this;
        }

        public RestaurantBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public RestaurantBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public RestaurantBuilder withProducts(Set<Product> products) {
            this.products = products;
            return this;
        }

        public RestaurantBuilder withAddress(Address address) {
            this.address = address;
            return this;
        }

        public RestaurantBuilder withPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public RestaurantBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public RestaurantBuilder withLogin(String login) {
            this.login = login;
            return this;
        }

        public RestaurantBuilder withEncodedPassword(String encodedPassword) {
            this.encodedPassword = encodedPassword;
            return this;
        }

        public RestaurantBuilder withCategory(RestaurantCategory category) {
            this.category = category;
            return this;
        }

        public RestaurantBuilder withImage(Image image) {
            this.image = image;
            return this;
        }

        public Restaurant build() {
            Restaurant restaurant = new Restaurant();
            restaurant.setId(id);
            restaurant.setCreatedOn(createdOn);
            restaurant.setName(name);
            restaurant.setDescription(description);
            restaurant.setProducts(products);
            restaurant.setAddress(address);
            restaurant.setPhoneNumber(phoneNumber);
            restaurant.setEmail(email);
            restaurant.setLogin(login);
            restaurant.setEncodedPassword(encodedPassword);
            restaurant.setCategory(category);
            restaurant.setImage(image);
            return restaurant;
        }
    }
}

package pl.kubson_eats.crud.exception;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.persistence.EntityNotFoundException;

@ControllerAdvice
public class ExceptionAdvice {

    @ResponseBody
    @ExceptionHandler(SameStatusException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String sameStatusHandler(SameStatusException e) {
        return e.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(LastPositionException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String lastPositionHandler(LastPositionException e) {
        return e.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String notFoundHandler(NotFoundException e) {
        return e.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(WrongIdException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String wrongIdExceptionHandler(WrongIdException e) {
        return e.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String entityNotFoundExceptionHandler(EntityNotFoundException e) {
        return e.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String validationExceptionHandler(MethodArgumentNotValidException e) {
        return e.getLocalizedMessage();
    }

    @ResponseBody
    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String dataIntegrityViolationException(DataIntegrityViolationException e) {
        return e.getMostSpecificCause().getMessage();
    }

    @ResponseBody
    @ExceptionHandler(InvalidDataAccessApiUsageException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String invalidDataAccessApiUsageExceptionHandler(InvalidDataAccessApiUsageException e) {
        return e.getMostSpecificCause().getMessage();
    }

    @ResponseBody
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String methodArgumentTypeMismatchExceptionHandler(MethodArgumentTypeMismatchException e) {
        return e.getMostSpecificCause().getMessage();
    }

    @ResponseBody
    @ExceptionHandler(PropertyReferenceException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String propertyReferenceExceptionHandler(PropertyReferenceException e) {
        return e.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(NullImageException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String nullImageExceptionHandler(NullImageException e) {
        return e.getMessage();
    }
}

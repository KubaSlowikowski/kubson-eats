package pl.kubson_eats.crud.product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.kubson_eats.crud.abstraction.AbstractController;
import pl.kubson_eats.utils.ProductCategory;

@RestController
@RequestMapping("/api/products")
public class ProductController extends AbstractController<ProductService, ProductDTO> {

    private final ProductService productService;

    protected ProductController(final ProductService productService) {
        super(productService);
        this.productService = productService;
    }

    @GetMapping(params = {"restaurantId"})
    public Page<ProductDTO> getAllByRestaurantId(@RequestParam("restaurantId") Long restaurantId,
                                                 @PageableDefault Pageable page) {
        return productService.findProductsByRestaurantId(restaurantId, page);
    }

    @GetMapping(params = {"category"})
    public Page<ProductDTO> getAllByCategory(@RequestParam("category") ProductCategory category,
                                             @PageableDefault Pageable pageable) {
        return productService.findAllByCategory(category, pageable);
    }

    @GetMapping(params = {"restaurantId", "category"})
    public Page<ProductDTO> getProductsByRestaurantAndCategory(@RequestParam("restaurantId") Long restaurantId,
                                                               @RequestParam("category") ProductCategory category,
                                                               @PageableDefault Pageable pageable) {
        return productService.findProductsByRestaurantIdAndCategory(restaurantId, category, pageable);
    }

    @GetMapping(path = "/categories", params = {"restaurantId"})
    public Page<ProductCategoryView> getAllAvailableCategoriesByRestaurantId(
            @RequestParam("restaurantId") Long restaurantId,
            @PageableDefault Pageable pageable) {
        return productService.findAvailableProductCategoriesByRestaurantId(restaurantId, pageable);
    }

    @GetMapping(path = "/{id}/restaurant/{restaurantId}/moveDown")
    public void moveDown(@PathVariable("id") Long id, @PathVariable("restaurantId") Long restaurantId) {
        productService.moveDown(id, restaurantId);
    }

    @GetMapping(path = "/{id}/restaurant/{restaurantId}/moveUp")
    public void moveUp(@PathVariable("id") Long id, @PathVariable("restaurantId") Long restaurantId) {
        productService.moveUp(id, restaurantId);
    }

    @PostMapping(path = "/uploadImage", params = {"productId"})
    public void uploadImage(@RequestParam("productId") Long productId,
                            @RequestParam("file") MultipartFile file) {
        productService.uploadImage(file, productId);
    }

    @DeleteMapping(path = "/deleteImage", params = {"productId"})
    public ProductDTO deleteImage(@RequestParam("productId") Long productId) {
        return productService.deleteImage(productId);
    }
}

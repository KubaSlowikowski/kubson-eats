package pl.kubson_eats.crud.address;

import org.springframework.stereotype.Repository;
import pl.kubson_eats.crud.abstraction.CommonRepository;

@Repository
public interface AddressRepository extends CommonRepository<Address> {
}

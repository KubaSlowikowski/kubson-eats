package pl.kubson_eats.crud.restaurant;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import pl.kubson_eats.crud.abstraction.AbstractService;
import pl.kubson_eats.crud.exception.NotFoundException;
import pl.kubson_eats.crud.exception.NullImageException;
import pl.kubson_eats.crud.image.ImageDTO;
import pl.kubson_eats.crud.image.ImageService;
import pl.kubson_eats.crud.image.ImageToUpload;

@Service
@Slf4j
public class RestaurantServiceImpl extends AbstractService<Restaurant, RestaurantDTO> implements RestaurantService {

    private final RestaurantRepository restaurantRepository;
    private final RestaurantMapper restaurantMapper;
    private final ImageService imageService;

    protected RestaurantServiceImpl(final RestaurantMapper restaurantMapper, final RestaurantRepository restaurantRepository, final ImageService imageService) {
        super(restaurantMapper, restaurantRepository);
        this.restaurantRepository = restaurantRepository;
        this.restaurantMapper = restaurantMapper;
        this.imageService = imageService;
    }

    @Override
    public Page<RestaurantView> findAllViews(final Pageable pageable, final String search) {
        Specification<Restaurant> spec = super.resolveSpecification(search);
        Page<RestaurantView> result = restaurantRepository.findAll(spec, RestaurantView.class, pageable);
        return new PageImpl<>(result.getContent(), pageable, result.getTotalElements());
    }

    @Override
    public RestaurantDTO findByName(final String name) {
        Restaurant restaurant = restaurantRepository.findByName(name).orElseThrow(() -> new NotFoundException(Restaurant.class.getSimpleName()));
        return restaurantMapper.toDto(restaurant);
    }

    @Override
    @Transactional
    public void /*CompletableFuture<Void>*/ uploadImage(final MultipartFile file, final Long restaurantId) {
//        return CompletableFuture.runAsync(() -> {
        ImageToUpload imageToUpload = new ImageToUpload();
        imageToUpload.setFile(file);
        RestaurantDTO restaurantDTO = findById(restaurantId);
        ImageDTO imageDTO = imageService.createNewImage(imageToUpload);
        restaurantDTO.setImageId(imageDTO.getId());
        RestaurantDTO updatedRestaurant = update(restaurantId, restaurantDTO);
        log.info("Image with id=" + updatedRestaurant.getImageId() +
                " assigned to Restaurant with id=" + updatedRestaurant.getId());
//        });
    }

    @Override
    public RestaurantDTO deleteImage(final Long restaurantId) {
        RestaurantDTO restaurantDTO = findById(restaurantId);
        if (restaurantDTO.getImageId() == null || restaurantDTO.getImageId() <= 0) {
            throw new NullImageException(Restaurant.class.getSimpleName());
        }
        imageService.delete(restaurantDTO.getImageId());
        restaurantDTO.setImageId(null);
        return update(restaurantId, restaurantDTO);
    }
}

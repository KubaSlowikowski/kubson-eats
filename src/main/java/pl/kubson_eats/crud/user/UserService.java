package pl.kubson_eats.crud.user;

import org.springframework.stereotype.Service;
import pl.kubson_eats.crud.abstraction.CommonService;

@Service
public interface UserService extends CommonService<UserDTO> {
}

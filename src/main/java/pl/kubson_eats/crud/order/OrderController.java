package pl.kubson_eats.crud.order;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import pl.kubson_eats.crud.abstraction.AbstractController;
import pl.kubson_eats.utils.OrderStatus;

@RestController
@RequestMapping("/api/orders")
public class OrderController extends AbstractController<OrderService, OrderDTO> {

    private final OrderService orderService;

    protected OrderController(final OrderService orderService) {
        super(orderService);
        this.orderService = orderService;
    }

    @GetMapping(path = "/restaurant/{id}")
    public Page<OrderDTO> getAllOrdersByRestaurantId(@PathVariable("id") Long restaurantId, @PageableDefault Pageable page) {
        return orderService.findAllByRestaurantId(restaurantId, page);
    }

    @GetMapping(path = "/restaurant/{id}/{status}")
    public Page<OrderDTO> getAllOrdersByRestuarantIdAndStatus(@PathVariable("id") Long restaurantId, @PathVariable("status") OrderStatus status) {
        return orderService.findAllbyRestauratnIdAndOrderStatus(restaurantId, status);
    }

    @GetMapping(path = "/user/{id}")
    public Page<OrderDTO> getOrdersByUserId(@PathVariable("id") Long userId, @PageableDefault Pageable page) {
        return orderService.findAllByUserId(userId, page);
    }

    @GetMapping(path = "/{status}")
    public Page<OrderDTO> getAllOrdersByStatus(@PathVariable("status") OrderStatus status, @PageableDefault Pageable pageable) {
        return orderService.findAllByOrderStatus(status, pageable);
    }

    @GetMapping(path = "/status/{id}")
    public Page<OrderDTO> getAllOrdersByUserIdAndStatus(@PathVariable("id") Long orderId, @RequestParam("status") OrderStatus status, @PageableDefault Pageable pageable) {
        return orderService.findOrdersByUserIdAndStatus(orderId, status, pageable);
    }

    @GetMapping(path = "status/{id}/change")
    public void changeStastus(@PathVariable("id") Long id, @RequestParam("status") OrderStatus status) {
        orderService.changeStatus(id, status);
    }
}

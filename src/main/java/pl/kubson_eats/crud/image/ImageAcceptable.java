package pl.kubson_eats.crud.image;

import org.springframework.web.multipart.MultipartFile;
import pl.kubson_eats.crud.abstraction.AbstractDto;

public interface ImageAcceptable<D extends AbstractDto> {

    //    @Async
    /*CompletableFuture<Void>*/ void uploadImage(MultipartFile file, Long entityId);

    //    @Async
    /*CompletableFuture<Void>*/ D deleteImage(Long entityId);
}

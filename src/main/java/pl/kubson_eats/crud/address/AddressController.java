package pl.kubson_eats.crud.address;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.kubson_eats.crud.abstraction.AbstractController;

@RestController
@RequestMapping("/api/addresses")
public class AddressController extends AbstractController<AddressService, AddressDTO> { //FIXME zobaczyc czy w ogole jest nam potrzebny ten kontroler

    protected AddressController(final AddressService service) {
        super(service);
    }
}

package pl.kubson_eats.crud.exception;

public class NotFoundException extends RuntimeException {
    public NotFoundException(String className) {
        super("Could not find " + className + "with given params");
    }

    public NotFoundException(Long id, String className) {
        super("Could not find " + className + " with id = " + id + ".");
    }
}

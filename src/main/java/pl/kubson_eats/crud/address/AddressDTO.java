package pl.kubson_eats.crud.address;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kubson_eats.crud.abstraction.AbstractDto;

import javax.validation.constraints.Pattern;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class AddressDTO extends AbstractDto {

    private Integer apartmentNumber;

    private Integer buildingNumber;

    private String street;

    private String city;

    private String country;

    @Pattern(regexp = "[0-9]{2}-[0-9]{3}", message = "User's zip code must not be null and be formatted properly")
    private String zipCode;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof AddressDTO)) return false;
        AddressDTO that = (AddressDTO) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(street, that.street) &&
                Objects.equals(city, that.city) &&
                Objects.equals(country, that.country) &&
                Objects.equals(buildingNumber, that.buildingNumber) &&
                Objects.equals(apartmentNumber, that.apartmentNumber) &&
                Objects.equals(zipCode, that.zipCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(street);
    }

    public static final class AddressDTOBuilder {
        private Long id;
        private Integer apartmentNumber;
        private Integer buildingNumber;
        private String street;
        private String city;
        private String country;
        private String zipCode;

        private AddressDTOBuilder() {
        }

        public static AddressDTOBuilder anAddressDTO() {
            return new AddressDTOBuilder();
        }

        public AddressDTOBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public AddressDTOBuilder withApartmentNumber(Integer apartmentNumber) {
            this.apartmentNumber = apartmentNumber;
            return this;
        }

        public AddressDTOBuilder withBuildingNumber(Integer buildingNumber) {
            this.buildingNumber = buildingNumber;
            return this;
        }

        public AddressDTOBuilder withStreet(String street) {
            this.street = street;
            return this;
        }

        public AddressDTOBuilder withCity(String city) {
            this.city = city;
            return this;
        }

        public AddressDTOBuilder withCountry(String country) {
            this.country = country;
            return this;
        }

        public AddressDTOBuilder withZipCode(String zipCode) {
            this.zipCode = zipCode;
            return this;
        }

        public AddressDTO build() {
            AddressDTO addressDTO = new AddressDTO();
            addressDTO.setId(id);
            addressDTO.setApartmentNumber(apartmentNumber);
            addressDTO.setBuildingNumber(buildingNumber);
            addressDTO.setStreet(street);
            addressDTO.setCity(city);
            addressDTO.setCountry(country);
            addressDTO.setZipCode(zipCode);
            return addressDTO;
        }
    }
}

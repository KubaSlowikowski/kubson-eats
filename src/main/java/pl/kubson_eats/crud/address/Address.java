package pl.kubson_eats.crud.address;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kubson_eats.crud.abstraction.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "addresses")
@NoArgsConstructor
@Getter
@Setter
public class Address extends AbstractEntity {

    private Integer apartmentNumber;

    private Integer buildingNumber;

    private String street;

    private String city;

    private String country;

    private String zipCode;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;
        Address address = (Address) o;
        return Objects.equals(getId(), address.getId()) &&
                Objects.equals(street, address.street) &&
                Objects.equals(city, address.city) &&
                Objects.equals(country, address.country) &&
                Objects.equals(getCreatedOn(), address.getCreatedOn()) &&
                Objects.equals(zipCode, address.zipCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public static final class AddressBuilder {
        private Long id;
        private LocalDateTime createdOn;
        private Integer apartmentNumber;
        private Integer buildingNumber;
        private String street;
        private String city;
        private String country;
        private String zipCode;

        private AddressBuilder() {
        }

        public static AddressBuilder anAddress() {
            return new AddressBuilder();
        }

        public AddressBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public AddressBuilder withCreatedOn(LocalDateTime createdOn) {
            this.createdOn = createdOn;
            return this;
        }

        public AddressBuilder withApartmentNumber(Integer apartmentNumber) {
            this.apartmentNumber = apartmentNumber;
            return this;
        }

        public AddressBuilder withBuildingNumber(Integer buildingNumber) {
            this.buildingNumber = buildingNumber;
            return this;
        }

        public AddressBuilder withStreet(String street) {
            this.street = street;
            return this;
        }

        public AddressBuilder withCity(String city) {
            this.city = city;
            return this;
        }

        public AddressBuilder withCountry(String country) {
            this.country = country;
            return this;
        }

        public AddressBuilder withZipCode(String zipCode) {
            this.zipCode = zipCode;
            return this;
        }

        public Address build() {
            Address address = new Address();
            address.setId(id);
            address.setCreatedOn(createdOn);
            address.setApartmentNumber(apartmentNumber);
            address.setBuildingNumber(buildingNumber);
            address.setStreet(street);
            address.setCity(city);
            address.setCountry(country);
            address.setZipCode(zipCode);
            return address;
        }
    }
}

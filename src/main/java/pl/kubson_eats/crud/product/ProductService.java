package pl.kubson_eats.crud.product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.kubson_eats.crud.abstraction.CommonService;
import pl.kubson_eats.crud.image.ImageAcceptable;
import pl.kubson_eats.utils.ProductCategory;

@Service
public interface ProductService extends CommonService<ProductDTO>, ImageAcceptable<ProductDTO> {
    Page<ProductDTO> findProductsByRestaurantId(Long restaurantId, Pageable pageable);

    Page<ProductDTO> findAllByCategory(ProductCategory category, Pageable pageable);

    Page<ProductDTO> findProductsByRestaurantIdAndCategory(Long restaurantId, ProductCategory category, Pageable pageable);

    Page<ProductCategoryView> findAvailableProductCategoriesByRestaurantId(Long restaurantId, Pageable pageable);

    void moveDown(Long id, Long restaurantId);

    void moveUp(Long id, Long restaurantId);
}

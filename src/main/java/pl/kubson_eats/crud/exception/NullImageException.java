package pl.kubson_eats.crud.exception;

public class NullImageException extends RuntimeException {
    public NullImageException(String className) {
        super(className + " has no images.");
    }
}

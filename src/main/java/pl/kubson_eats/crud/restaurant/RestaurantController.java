package pl.kubson_eats.crud.restaurant;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.kubson_eats.crud.abstraction.AbstractController;

@RestController
@RequestMapping("/api/restaurants")
public class RestaurantController extends AbstractController<RestaurantService, RestaurantDTO> {

    private final RestaurantService restaurantService;

    protected RestaurantController(final RestaurantService restaurantService) {
        super(restaurantService);
        this.restaurantService = restaurantService;
    }

    @GetMapping(params = {"viewsOnly"})
    public Page<RestaurantView> findAllViews(@PageableDefault Pageable pageable, @RequestParam(value = "search", required = false) String search) {
        return restaurantService.findAllViews(pageable, search);
    }

    @GetMapping(params = {"name"})
    public RestaurantDTO findByName(@RequestParam("name") String name) {
        return restaurantService.findByName(name);
    }

    @PostMapping(path = "/uploadImage", params = {"restaurantId"})
    public void uploadImage(@RequestParam("restaurantId") Long restaurantId,
                            @RequestParam("file") MultipartFile file) {
        restaurantService.uploadImage(file, restaurantId);
    }

    @DeleteMapping(path = "/deleteImage", params = {"restaurantId"})
    public RestaurantDTO deleteImage(@RequestParam("restaurantId") Long restaurantId) {
        return restaurantService.deleteImage(restaurantId);
    }
}

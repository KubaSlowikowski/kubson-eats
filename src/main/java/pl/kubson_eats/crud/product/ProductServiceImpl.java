package pl.kubson_eats.crud.product;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import pl.kubson_eats.crud.abstraction.AbstractService;
import pl.kubson_eats.crud.exception.LastPositionException;
import pl.kubson_eats.crud.exception.NotFoundException;
import pl.kubson_eats.crud.exception.NullImageException;
import pl.kubson_eats.crud.image.ImageDTO;
import pl.kubson_eats.crud.image.ImageService;
import pl.kubson_eats.crud.image.ImageToUpload;
import pl.kubson_eats.crud.restaurant.Restaurant;
import pl.kubson_eats.crud.restaurant.RestaurantRepository;
import pl.kubson_eats.utils.ProductCategory;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductServiceImpl extends AbstractService<Product, ProductDTO> implements ProductService {

    private final ProductMapper productMapper;
    private final ProductRepository productRepository;
    private final RestaurantRepository restaurantRepository;
    private final ImageService imageService;

    protected ProductServiceImpl(final ProductMapper productMapper,
                                 final ProductRepository productRepository,
                                 final RestaurantRepository restaurantRepository,
                                 final ImageService imageService) {
        super(productMapper, productRepository);
        this.productMapper = productMapper;
        this.productRepository = productRepository;
        this.restaurantRepository = restaurantRepository;
        this.imageService = imageService;
    }

    @Override
    public Page<ProductDTO> findProductsByRestaurantId(final Long restaurantId, final Pageable pageable) {
        checkIfRestaurantExists(restaurantId);
        List<Product> products = productRepository.findAllByRestaurantId(restaurantId, pageable);
        return new PageImpl<>(productMapper.toListDto(products), pageable, products.size());
    }

    @Override
    public Page<ProductDTO> findAllByCategory(ProductCategory category, Pageable pageable) {
        List<Product> products = productRepository.findAllByCategoryOrderByPosition(category, pageable);
        return new PageImpl<>(productMapper.toListDto(products), pageable, products.size());
    }

    @Override
    public Page<ProductDTO> findProductsByRestaurantIdAndCategory(final Long restaurantId,
                                                                  final ProductCategory category,
                                                                  final Pageable pageable) {
        checkIfRestaurantExists(restaurantId);
        List<Product> products = productRepository.findAllByRestaurantIdAndCategoryOrderByPosition(restaurantId, category, pageable);
        return new PageImpl<>(productMapper.toListDto(products), pageable, products.size());
    }

    @Override
    public Page<ProductCategoryView> findAvailableProductCategoriesByRestaurantId(final Long restaurantId, final Pageable pageable) {
        checkIfRestaurantExists(restaurantId);
        List<ProductCategoryView> categories = productRepository.findAvailableProductCategoriesByRestaurantId(restaurantId, pageable).stream().distinct().collect(Collectors.toList());
        return new PageImpl<>(categories, pageable, categories.size());
    }

    @Override
    @Transactional
    public void moveDown(Long id, Long restaurantId) {
        Product previousProduct = productRepository.findByIdAndRestaurantId(id, restaurantId).orElseThrow(NullPointerException::new);
        int previousPosition = previousProduct.getPosition();
        Product nextProduct = productRepository
                .findByRestaurantIdAndPositionAndCategory(restaurantId, previousPosition + 1, previousProduct.getCategory())
                .orElseThrow(LastPositionException::new);
        previousProduct.setPosition(previousPosition + 1);
        nextProduct.setPosition(previousPosition);
        productRepository.save(previousProduct);
        productRepository.save(nextProduct);
    }

    @Override
    @Transactional
    public void moveUp(Long id, Long restaurantId) {
        Product nextProduct = productRepository.findByIdAndRestaurantId(id, restaurantId).orElseThrow(NullPointerException::new);
        int nextPosition = nextProduct.getPosition();
        Product previousProduct = productRepository.findByRestaurantIdAndPositionAndCategory(restaurantId, nextPosition - 1, nextProduct.getCategory())
                .orElseThrow(LastPositionException::new);
        nextProduct.setPosition(nextPosition - 1);
        previousProduct.setPosition(nextPosition);
        productRepository.save(nextProduct);
        productRepository.save(previousProduct);
    }

    @Override
    public ProductDTO save(final ProductDTO dto) {
        getLastProductPosition(dto);
        return super.save(dto);
    }

    @Override
    public ProductDTO update(final Long id, final ProductDTO dto) {
        getLastProductPosition(dto);
        return super.update(id, dto);
    }

    @Override
    @Transactional
    public /*CompletableFuture<Void>*/ void uploadImage(final MultipartFile file, final Long productId) {
//        return CompletableFuture.runAsync(() -> {
        ImageToUpload imageToUpload = new ImageToUpload();
        imageToUpload.setFile(file);
        ProductDTO productDTO = findById(productId);
        if (productDTO.getImageId() != null && productDTO.getImageId() > 0) {
            imageService.delete(productDTO.getImageId());
        }
        ImageDTO imageDTO = imageService.createNewImage(imageToUpload);
        productDTO.setImageId(imageDTO.getId());
        ProductDTO updatedProduct = update(productId, productDTO);
        log.info("Image with id=" + updatedProduct.getImageId() + " assigned to Product with id=" + updatedProduct.getId());
//        });
    }

    @Override
    public /*CompletableFuture<Void>*/ ProductDTO deleteImage(final Long productId) {
        ProductDTO productDTO = findById(productId);
        if (productDTO.getImageId() == null || productDTO.getImageId() <= 0) {
            throw new NullImageException(Product.class.getSimpleName());
        }
        imageService.delete(productDTO.getImageId());
        productDTO.setImageId(null);
        return update(productId, productDTO);
    }

    private void getLastProductPosition(final ProductDTO dto) {
        if (dto.getPosition() == null) {
            Integer lastPosition = productRepository.findLastPosition();
            if (lastPosition != null) {
                dto.setPosition(lastPosition + 1);
            } else {
                dto.setPosition(1);
            }
        }
    }

    private void checkIfRestaurantExists(final Long restaurantId) {
        restaurantRepository.findById(restaurantId).orElseThrow(() -> new NotFoundException(restaurantId, Restaurant.class.getSimpleName()));
    }
}

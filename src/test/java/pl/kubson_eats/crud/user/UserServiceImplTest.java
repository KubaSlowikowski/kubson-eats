package pl.kubson_eats.crud.user;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import pl.kubson_eats.crud.abstraction.*;

public class UserServiceImplTest extends AbstractServiceTest<User , UserDTO> {

    @InjectMocks
    private UserServiceImpl userService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private UserMapper userMapper;

    @Override
    public AbstractService<User, UserDTO> getService(){
        return userService;
    }

    @Override
    public CommonRepository<User> getRepostiory() {
        return userRepository;
    }


    @Override
    public CommonMapper<User, UserDTO> getMapper(){
        return userMapper;
    }
    @Override
    public Class<UserDTO> getDtoClass(){
        return UserDTO.class;
    }
    @Override
    public Class<User> getEntityClass(){
        return User.class;
    }

}
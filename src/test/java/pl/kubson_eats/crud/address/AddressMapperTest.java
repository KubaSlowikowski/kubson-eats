package pl.kubson_eats.crud.address;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kubson_eats.utils.Utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@RunWith(MockitoJUnitRunner.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AddressMapperTest {

    private final AddressMapper addressMapper = AddressMapperImpl.INSTANCE;

    @Test
    void should_map_address_to_addressDto() {
        //given
        Address address = Utils.getAddress();
        AddressDTO addressDTO = Utils.getAddressDTO();

        //when
        AddressDTO result = addressMapper.toDto(address);

        //then
        assertEquals(addressDTO, result);
    }

    @Test
    void shouldnt_map_address_to_addressDto() {
        //given
        Address address = Utils.getAddress();
        AddressDTO addressDTO = Utils.getAddressDTO();
        addressDTO.setCity("123retgr");

        //when
        AddressDTO result = addressMapper.toDto(address);

        //then
        assertNotEquals(addressDTO, result);
    }

    @Test
    void should_map_addressDto_to_address() {
        //given
        AddressDTO addressDTO = Utils.getAddressDTO();
        Address address = Utils.getAddress();
        address.setCreatedOn(null);

        //when
        Address result = addressMapper.fromDto(addressDTO);

        //then
        assertEquals(address, result);
    }

    @Test
    void shouldnt_map_addressDto_to_address() {
        //given
        AddressDTO addressDTO = Utils.getAddressDTO();
        Address address = Utils.getAddress();
        address.setCreatedOn(null);
        address.setCity("12312fi3rht2");

        //when
        Address result = addressMapper.fromDto(addressDTO);

        //then
        assertNotEquals(address, result);
    }
}

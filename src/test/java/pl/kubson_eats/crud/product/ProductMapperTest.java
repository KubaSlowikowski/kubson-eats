package pl.kubson_eats.crud.product;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kubson_eats.utils.Utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@RunWith(MockitoJUnitRunner.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ProductMapperTest {

    ProductMapper productMapper = ProductMapperImpl.INSTANCE;

    @Test
    void should_map_product_to_productDto() {
        //given
        Product product = Utils.getProduct();
        ProductDTO productDTO = Utils.getProductDTO();

        //when
        ProductDTO result = productMapper.toDto(product);

        //then
        assertEquals(productDTO, result);
    }

    @Test
    void shouldnt_map_product_to_productDto() {
        //given
        Product product = Utils.getProduct();
        ProductDTO productDTO = Utils.getProductDTO();
        productDTO.setDescription("1232132112f31f");

        //when
        ProductDTO result = productMapper.toDto(product);

        //then
        assertNotEquals(productDTO, result);
    }

    @Test
    void should_map_productDto_to_product() {
        //given
        Product product = Utils.getProduct();
        ProductDTO productDTO = Utils.getProductDTO();

        //when
        Product result = productMapper.fromDto(productDTO);

        //then
        assertEquals(product, result);
    }

    @Test
    void shouldnt_map_productDto_to_product() {
        //given
        Product product = Utils.getProduct();
        ProductDTO productDTO = Utils.getProductDTO();
        productDTO.setDescription("21p3u18r723g582");

        //when
        Product result = productMapper.fromDto(productDTO);

        //then
        assertNotEquals(product, result);
    }
}
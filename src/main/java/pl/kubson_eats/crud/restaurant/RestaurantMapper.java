package pl.kubson_eats.crud.restaurant;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import pl.kubson_eats.crud.abstraction.CommonMapper;
import pl.kubson_eats.crud.address.AddressMapper;
import pl.kubson_eats.crud.image.ImageMapper;
import pl.kubson_eats.crud.product.ProductMapper;

@Mapper(componentModel = "spring", uses = {ProductMapper.class, AddressMapper.class, ImageMapper.class})
public interface RestaurantMapper extends CommonMapper<Restaurant, RestaurantDTO> {
    RestaurantMapper INSTANCE = Mappers.getMapper(RestaurantMapper.class);

    @Override
    @Mapping(source = "encodedPassword", target = "password")
    @Mapping(source = "image.id", target = "imageId")
    RestaurantDTO toDto(Restaurant restaurant);

    @Override
    @Mapping(source = "password", target = "encodedPassword")
    @Mapping(source = "imageId", target = "image.id")
    Restaurant fromDto(RestaurantDTO dto);

    @AfterMapping
    default void afterMappingFromDto(RestaurantDTO source, @MappingTarget Restaurant target) {
        if (source.getImageId() == null) {
            target.setImage(null);
        }
    }
}

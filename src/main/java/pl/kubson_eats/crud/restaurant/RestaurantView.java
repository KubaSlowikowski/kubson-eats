package pl.kubson_eats.crud.restaurant;

import pl.kubson_eats.utils.RestaurantCategory;

public interface RestaurantView { //Interface-Based Projection
    String getName();

    String getDescription();

    RestaurantCategory getCategory();
}

package pl.kubson_eats.crud.abstraction;

import com.google.common.base.Joiner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.kubson_eats.crud.exception.NotFoundException;
import pl.kubson_eats.crud.exception.WrongIdException;
import pl.kubson_eats.crud.searchSpecification.CommonSearchSpecificationBuilder;
import pl.kubson_eats.crud.searchSpecification.SearchOperation;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public abstract class AbstractService<E extends AbstractEntity, D extends AbstractDto> implements CommonService<D> {

    private final CommonMapper<E, D> commonMapper;
    private final CommonRepository<E> commonRepository;

    protected AbstractService(final CommonMapper<E, D> commonMapper, final CommonRepository<E> commonRepository) {
        this.commonMapper = commonMapper;
        this.commonRepository = commonRepository;
    }

    @Override
    @Transactional
    public Page<D> getAll(Pageable pageable, String search) {
        Specification<E> spec = resolveSpecification(search);
        Page<E> result = commonRepository.findAll(spec, pageable);
        List<D> content = commonMapper.toListDto(result.getContent());
        return new PageImpl<>(content, pageable, result.getTotalElements());
    }

    @Override
    @Transactional
    public D findById(Long id) {
        E entity = getEntityById(id);
        return commonMapper.toDto(entity);
    }

    @Transactional
    public E getEntityById(Long id) {
        if (id <= 0) {
            throw new WrongIdException(id);
        }
        return commonRepository.findById(id).orElseThrow(() -> new NotFoundException(id, "entityName"));//FIXME
    }

    @Override
    @Transactional
    public D save(D dto) {
        E entity = commonMapper.fromDto(dto);
        E savedResult = commonRepository.saveAndFlush(entity);
        return commonMapper.toDto(savedResult);
    }

    @Override
    @Transactional
    public D update(Long id, D dto) {
        if (id < 1 || !dto.getId().equals(id)) {
            throw new WrongIdException(id);
        }
        getEntityById(id);
        E entity = commonMapper.fromDto(dto);
        E result = commonRepository.saveAndFlush(entity);
        return commonMapper.toDto(result);
    }

    @Override
    @Transactional
    public D delete(Long id) {
        E entity = getEntityById(id);
        commonRepository.deleteById(entity.getId());
        return commonMapper.toDto(entity);
    }

    protected Specification<E> resolveSpecification(String searchParameters) {
        if (searchParameters == null || searchParameters.isEmpty()) {
            return null;
        }

        CommonSearchSpecificationBuilder<E> builder = new CommonSearchSpecificationBuilder<>();

        final String operationSetExper = Joiner.on("|")
                .join(SearchOperation.SIMPLE_OPERATION_SET);

        Pattern pattern = Pattern.compile(
                "(\\p{Punct}?)" +
                        "(\\w+?)" +
                        "([.]\\w+?)?" +
                        "(" + operationSetExper + ")" +
                        "(\\p{Punct}?)" +
                        "(\\w+?)" +
                        "(\\p{Punct}?),",
                Pattern.UNICODE_CHARACTER_CLASS); //2nd argument is to support non-English systems
        /*
        In pattern:
            group1 - orPredicate
            group2 - key (field)
            group3 - if '.' is present -> '.' + inner field
            group4 - operation
            group5 - prefix
            group6 - value
            group7 - suffix
         */

        Matcher matcher = pattern.matcher(searchParameters + ",");

        while (matcher.find()) {
            builder.with(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4),
                    matcher.group(6), matcher.group(5), matcher.group(7));
        }

        return builder.build();
    }
}

package pl.kubson_eats.utils;

import pl.kubson_eats.crud.address.Address;
import pl.kubson_eats.crud.address.AddressDTO;
import pl.kubson_eats.crud.order.Order;
import pl.kubson_eats.crud.order.OrderDTO;
import pl.kubson_eats.crud.product.Product;
import pl.kubson_eats.crud.product.ProductDTO;
import pl.kubson_eats.crud.restaurant.Restaurant;
import pl.kubson_eats.crud.restaurant.RestaurantDTO;
import pl.kubson_eats.crud.user.User;
import pl.kubson_eats.crud.user.UserDTO;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Utils {

    private static final Long ID = 1L;
    private static final LocalDateTime TIME = LocalDateTime.of(2020, 12, 13, 10, 59);
    private static final String ORDER_COMMENT = "comment";
    private static final OrderStatus ORDER_STATUS = OrderStatus.READY;

    public static Order getOrder() {
        return Order.OrderBuilder.anOrder()
                .withId(ID)
                .withOrderComment(ORDER_COMMENT)
                .withUser(User.UserBuilder.anUser().withId(ID).build())
                .withCreatedOn(TIME)
                .withOrderStatus(ORDER_STATUS)
                .build();
    }

    public static OrderDTO getOrderDTO() {
        return OrderDTO.OrderDTOBuilder.anOrderDTO()
                .withId(ID)
                .withOrderComment(ORDER_COMMENT)
                .withUserId(ID)
                .withOrderStatus(ORDER_STATUS)
                .build();
    }

    private static final BigDecimal PRICE = new BigDecimal(100L);
    private static final String DESCRIPTION = "description";
    private static final Integer POSITION = 1;
    private static final ProductCategory PRODUCT_CATEGORY = ProductCategory.ALCOHOL;

    public static Product getProduct() {
        return Product.ProductBuilder.aProduct()
                .withId(ID)
                .withPrice(PRICE)
                .withDescription(DESCRIPTION)
                .withPosition(POSITION)
                .withCategory(PRODUCT_CATEGORY)
                .withRestaurant(Restaurant.RestaurantBuilder.aRestaurant().withId(ID).build())
                .build();
    }

    public static ProductDTO getProductDTO() {
        return ProductDTO.ProductDTOBuilder.aProductDTO()
                .withId(ID)
                .withPrice(PRICE)
                .withDescription(DESCRIPTION)
                .withPosition(POSITION)
                .withCategory(PRODUCT_CATEGORY)
                .withRestaurantId(ID)
                .build();
    }

    private static final String NAME = "name";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String PHONE_NUMBER = "123456789";
    private static final String EMAIL = "email@email.com";

    public static Restaurant getRestaurant() {
        return Restaurant.RestaurantBuilder.aRestaurant()
                .withId(ID)
                .withName(NAME)
                .withDescription(DESCRIPTION)
                .withLogin(LOGIN)
                .withPhoneNumber(PHONE_NUMBER)
                .withEmail(EMAIL)
                .withEncodedPassword(PASSWORD)
                .build();
    }

    public static RestaurantDTO getRestaurantDto() {
        return RestaurantDTO.RestaurantDTOBuilder.aRestaurantDTO()
                .withId(ID)
                .withName(NAME)
                .withDescription(DESCRIPTION)
                .withLogin(LOGIN)
                .withPhoneNumber(PHONE_NUMBER)
                .withEmail(EMAIL)
                .withPassword(PASSWORD)
                .build();
    }


    public static User getUser() {
        return User.UserBuilder.anUser()
                .withId(ID)
                .withName(NAME)
                .withLogin(LOGIN)
                .withEncodedPassword(PASSWORD)
                .build();
    }

    public static UserDTO getUserDTO() {
        return UserDTO.UserDTOBuilder.anUserDTO()
                .withId(ID)
                .withName(NAME)
                .withLogin(LOGIN)
                .withPassword(PASSWORD)
                .build();
    }

    private static final String CITY = "city";
    private static final String COUNTRY = "country";
    private static final String STREET = "street";

    public static Address getAddress() {
        return Address.AddressBuilder.anAddress()
                .withId(ID)
                .withCity(CITY)
                .withStreet(STREET)
                .withCountry(COUNTRY)
                .build();
    }

    public static AddressDTO getAddressDTO() {
        return AddressDTO.AddressDTOBuilder.anAddressDTO()
                .withId(ID)
                .withCity(CITY)
                .withStreet(STREET)
                .withCountry(COUNTRY)
                .build();
    }
}

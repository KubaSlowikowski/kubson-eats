package pl.kubson_eats.crud.order;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import pl.kubson_eats.crud.abstraction.CommonMapper;
import pl.kubson_eats.crud.product.ProductMapper;

@Mapper(componentModel = "spring", uses = {ProductMapper.class})
public interface OrderMapper extends CommonMapper<Order, OrderDTO> {
    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);

    @Override
    @Mapping(source = "user.id", target = "userId")
    OrderDTO toDto(Order order);

    @Override
    @Mapping(source = "userId", target = "user.id")
    Order fromDto(OrderDTO dto);
}

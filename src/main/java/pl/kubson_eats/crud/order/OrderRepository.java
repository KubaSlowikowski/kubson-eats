package pl.kubson_eats.crud.order;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;
import pl.kubson_eats.crud.abstraction.CommonRepository;
import pl.kubson_eats.crud.product.Product;
import pl.kubson_eats.utils.OrderStatus;

import java.util.Collection;
import java.util.List;

@Repository
public interface OrderRepository extends CommonRepository<Order> {
    Page<Order> findAllByProductsIn(Collection<Product> products, Pageable pageable);

    Page<Order> findAllByProductsInAndOrderStatus(Collection<Product> products, OrderStatus status, Pageable pageable);

    List<Order> findAllByUserId(Long userId, Pageable pageable);

    List<Order> findAllByOrderStatus(OrderStatus status, Pageable pageable);

    List<Order> findAllByUserIdAndOrderStatus(Long userId, OrderStatus status, Pageable pageable);

    @Override
    @EntityGraph(attributePaths = {"products", "user"})
    Page<Order> findAll(Specification<Order> specification, Pageable pageable);
}

package pl.kubson_eats.crud.abstraction;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public abstract class AbstractServiceTest<Entity extends AbstractEntity, DTO extends AbstractDto> {

    public abstract AbstractService<Entity, DTO> getService();

    public abstract CommonRepository<Entity> getRepostiory();

    public abstract CommonMapper<Entity, DTO> getMapper();

    @Mock
    EntityManager entityManager;

    public abstract Class<DTO> getDtoClass();

    public abstract Class<Entity> getEntityClass();

    @Captor
    ArgumentCaptor<DTO> dtoCaptor;

    //TODO Whtrite correct tests
    @Test
    public void getAll_getAllEntityAndReturnDto() {
       /* List<Entity> returnedEntity = asList(
                mock(getEntityClass()),
                mock(getEntityClass()));
        List<DTO> returnedDtos = asList(
                mock(getDtoClass()),
                mock(getDtoClass()));
        when(getRepostiory().findAll()).thenReturn(returnedEntity);
        when(getMapper().toListDto(returnedEntity)).thenReturn(returnedDtos);
        Page<DTO> result = getService().getAll(Pageable.unpaged(), "");

        verify(getRepostiory().findAll());
        verify(getMapper().toListDto(returnedEntity));
        assertThat(result).isEqualTo(returnedDtos);
*/
    }

    @Test
    public void findById() {
    }

    @Test
    public void getEntityById() {
    }

    @Test
    public void save_savesAndReturnsMappedEntity() {
    }

    @Test
    public void update_savesUpdatedProductAndReturnsDto() {
    }

    @Test
    public void delete_deletesById() {
        getRepostiory().deleteById(1L);

        verify(getRepostiory()).deleteById(1L);
    }
}
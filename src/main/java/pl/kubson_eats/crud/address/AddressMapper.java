package pl.kubson_eats.crud.address;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.kubson_eats.crud.abstraction.CommonMapper;

@Mapper(componentModel = "spring")
public interface AddressMapper extends CommonMapper<Address, AddressDTO> {
    AddressMapper INSTANCE = Mappers.getMapper(AddressMapper.class);
}

package pl.kubson_eats.crud.product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.kubson_eats.crud.abstraction.CommonRepository;
import pl.kubson_eats.utils.ProductCategory;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends CommonRepository<Product> {
    List<Product> findAllByRestaurantId(Long restaurantId, Pageable pageable);

    @Query(value = "SELECT max(position) FROM public.products", nativeQuery = true)
    Integer findLastPosition();

    /*Optional<Product> findByPositionAndCategory(int position, ProductCategory category);*/

    Optional<Product> findByIdAndRestaurantId(Long id, Long restaurantId);

    Optional<Product> findByRestaurantIdAndPositionAndCategory(long restaurantId, int position, ProductCategory category);

    List<Product> findAllByCategoryOrderByPosition(ProductCategory category, Pageable pageable);

    Optional<Product> findByPositionAndCategory(int position, ProductCategory category);

    List<Product> findAllByRestaurantIdAndCategoryOrderByPosition(Long restaurantId, ProductCategory category, Pageable pageable);

    List<ProductCategoryView> findAvailableProductCategoriesByRestaurantId(Long restaurantId, Pageable pageable);

    @Override
    @EntityGraph(attributePaths = {"restaurant"})
    Page<Product> findAll(Specification<Product> specification, Pageable pageable);
}

package pl.kubson_eats.crud.exception;

import pl.kubson_eats.utils.OrderStatus;

public class SameStatusException extends RuntimeException {
    public SameStatusException() {
        super("The same Status");
    }

    public SameStatusException(Long id, OrderStatus status) {
        super("ID= " + id + " Have the same status = " + status);
    }
}

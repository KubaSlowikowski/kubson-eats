package pl.kubson_eats.crud.product;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import pl.kubson_eats.crud.abstraction.CommonMapper;
import pl.kubson_eats.crud.image.ImageMapper;

@Mapper(componentModel = "spring", uses = {ImageMapper.class})
public interface ProductMapper extends CommonMapper<Product, ProductDTO> {
    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    @Override
    @Mapping(source = "restaurant.id", target = "restaurantId")
    @Mapping(source = "image.id", target = "imageId")
    ProductDTO toDto(Product product);

    @Override
    @Mapping(source = "restaurantId", target = "restaurant.id")
    @Mapping(source = "imageId", target = "image.id")
    Product fromDto(ProductDTO dto);

    @AfterMapping
    default void afterMappingFromDto(ProductDTO source, @MappingTarget Product target) {
        if (source.getImageId() == null) {
            target.setImage(null);
        }
    }
}

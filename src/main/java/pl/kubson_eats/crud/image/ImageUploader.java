package pl.kubson_eats.crud.image;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import lombok.SneakyThrows;

import java.util.Map;

public class ImageUploader { //bean defined in CloudinaryConfig

    private final Cloudinary cloudinary;

    public ImageUploader(final Cloudinary cloudinary) {
        this.cloudinary = cloudinary;
    }

    @SneakyThrows
    public Map uploadImage(byte[] bytes) {
        return cloudinary.uploader().upload(
                bytes,
                ObjectUtils.asMap("resource_type", "auto")
        );
    }

    @SneakyThrows
    public Map deleteImage(String publicId, Map options) {
        return cloudinary.uploader().destroy(publicId, options);
    }

}

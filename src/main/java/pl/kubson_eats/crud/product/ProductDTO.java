package pl.kubson_eats.crud.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kubson_eats.crud.abstraction.AbstractDto;
import pl.kubson_eats.utils.ProductCategory;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class ProductDTO extends AbstractDto {

    @NotBlank(message = "Product's name must not be null or empty")
    private String name;

    @NotNull(message = "Product's name must not be null or empty")
    private BigDecimal price;

    @NotBlank(message = "Product's description must not be null or empty")
    private String description;

    @NotNull(message = "Product's must belong to a restaurant")
    private Long restaurantId;

    @Positive
    private Integer position;

    @NotNull(message = "Product's category must not be null")
    private ProductCategory category;

    @PositiveOrZero
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long imageId;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductDTO)) return false;
        ProductDTO that = (ProductDTO) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(name, that.name) &&
                Objects.equals(price, that.price) &&
                Objects.equals(description, that.description) &&
                Objects.equals(position, that.position) &&
                Objects.equals(category, that.category) &&
                Objects.equals(imageId, that.imageId) &&
                Objects.equals(restaurantId, that.restaurantId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public static final class ProductDTOBuilder {
        private Long id;
        private String name;
        private BigDecimal price;
        private String description;
        private Long restaurantId;
        private Integer position;
        private ProductCategory category;

        private ProductDTOBuilder() {
        }

        public static ProductDTOBuilder aProductDTO() {
            return new ProductDTOBuilder();
        }

        public ProductDTOBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public ProductDTOBuilder withPrice(BigDecimal price) {
            this.price = price;
            return this;
        }

        public ProductDTOBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public ProductDTOBuilder withRestaurantId(Long restaurantId) {
            this.restaurantId = restaurantId;
            return this;
        }

        public ProductDTOBuilder withPosition(Integer position) {
            this.position = position;
            return this;
        }

        public ProductDTOBuilder withCategory(ProductCategory category) {
            this.category = category;
            return this;
        }

        public ProductDTOBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public ProductDTO build() {
            ProductDTO productDTO = new ProductDTO();
            productDTO.setName(name);
            productDTO.setPrice(price);
            productDTO.setDescription(description);
            productDTO.setRestaurantId(restaurantId);
            productDTO.setPosition(position);
            productDTO.setCategory(category);
            productDTO.setId(id);
            return productDTO;
        }
    }
}

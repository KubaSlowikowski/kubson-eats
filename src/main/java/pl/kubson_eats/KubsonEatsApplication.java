package pl.kubson_eats;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KubsonEatsApplication {

    public static void main(String[] args) {
        SpringApplication.run(KubsonEatsApplication.class, args);
    }

}

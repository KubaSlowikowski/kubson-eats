package pl.kubson_eats.crud.restaurant;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.kubson_eats.crud.abstraction.CommonService;
import pl.kubson_eats.crud.image.ImageAcceptable;

@Service
public interface RestaurantService extends CommonService<RestaurantDTO>, ImageAcceptable<RestaurantDTO> {
    Page<RestaurantView> findAllViews(Pageable pageable, String search);

    RestaurantDTO findByName(String name);

}

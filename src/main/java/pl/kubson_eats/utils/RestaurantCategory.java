package pl.kubson_eats.utils;

public enum RestaurantCategory {
    ORIENT,
    ITALIAN,
    TURKISH,
    ASIAN,
    JAPAN,
    MEXICAN
}

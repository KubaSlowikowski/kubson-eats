package pl.kubson_eats.crud.image;

import org.springframework.stereotype.Service;

@Service
public interface ImageService {

    ImageDTO createNewImage(ImageToUpload imageToUpload);

    ImageDTO findById(Long id);

    void delete(Long id);
}

package pl.kubson_eats.crud.image;

import com.cloudinary.Singleton;
import com.cloudinary.StoredFile;
import com.cloudinary.Transformation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public class ImageToUpload extends StoredFile {

    @NotEmpty
    private MultipartFile file;

    public String getUrl() {
        if (version != null && format != null && publicId != null) {
            return Singleton.getCloudinary().url()
                    .resourceType(resourceType)
                    .type(type)
                    .format(format)
                    .version(version)
                    .generate(publicId);
        } else return null;
    }

    public String getThumbnailUrl() {
        if (version != null && format != null && publicId != null) {
            return Singleton.getCloudinary().url().format(format)
                    .resourceType(resourceType)
                    .type(type)
                    .version(version).transformation(new Transformation().width(150).height(150).crop("fit"))
                    .generate(publicId);
        } else return null;
    }

    public void setParams(Map uploadResult) {
        setPublicId((String) uploadResult.get("public_id"));

        Object version = uploadResult.get("version");
        if (version instanceof Integer) {
            setVersion(Long.valueOf((Integer) version));
        } else {
            setVersion((Long) version);
        }

        setSignature((String) uploadResult.get("signature"));
        setFormat((String) uploadResult.get("format"));
        setResourceType((String) uploadResult.get("resource_type"));
    }

    public String getComputedSignature() {
        return getComputedSignature(Singleton.getCloudinary());
    }

    public boolean validSignature() {
        return getComputedSignature().equals(signature);
    }
}

package pl.kubson_eats.crud.restaurant;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import pl.kubson_eats.crud.address.AddressMapper;
import pl.kubson_eats.crud.product.ProductMapper;
import pl.kubson_eats.utils.Utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@RunWith(MockitoJUnitRunner.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RestaurantMapperTest {

    @InjectMocks
    RestaurantMapper restaurantMapper = RestaurantMapperImpl.INSTANCE;

    @BeforeAll
    void init() {
        ProductMapper productMapper = Mappers.getMapper(ProductMapper.class);
        ReflectionTestUtils.setField(restaurantMapper, "productMapper", productMapper);
        AddressMapper addressMapper = Mappers.getMapper(AddressMapper.class);
        ReflectionTestUtils.setField(restaurantMapper, "addressMapper", addressMapper);
    }

    @Test
    void should_map_restaurant_to_restaurantDto() {
        //given
        Restaurant restaurant = Utils.getRestaurant();
        RestaurantDTO restaurantDTO = Utils.getRestaurantDto();

        //when
        RestaurantDTO result = restaurantMapper.toDto(restaurant);

        //then
        assertEquals(restaurantDTO, result);
    }

    @Test
    void shouldnt_map_restaurant_to_restaurantDto() {
        //given
        Restaurant restaurant = Utils.getRestaurant();
        RestaurantDTO restaurantDTO = Utils.getRestaurantDto();
        restaurantDTO.setName("weurthie4tvr");

        //when
        RestaurantDTO result = restaurantMapper.toDto(restaurant);

        //then
        assertNotEquals(restaurantDTO, result);
    }

    @Test
    void should_map_restaurantDto_to_restaurant() {
        //given
        Restaurant restaurant = Utils.getRestaurant();
        RestaurantDTO restaurantDTO = Utils.getRestaurantDto();

        //when
        Restaurant result = restaurantMapper.fromDto(restaurantDTO);

        //then
        assertEquals(restaurant, result);
    }

}

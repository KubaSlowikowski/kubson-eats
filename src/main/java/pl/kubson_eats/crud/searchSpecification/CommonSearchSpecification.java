package pl.kubson_eats.crud.searchSpecification;

import org.springframework.data.jpa.domain.Specification;
import pl.kubson_eats.crud.abstraction.AbstractEntity;
import pl.kubson_eats.crud.address.Address;
import pl.kubson_eats.crud.restaurant.Restaurant;
import pl.kubson_eats.crud.user.User;

import javax.persistence.Entity;
import javax.persistence.criteria.*;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Optional;

public class CommonSearchSpecification<E extends AbstractEntity> implements Specification<E> {
    private final transient SearchCriteria criteria;

    public CommonSearchSpecification(final SearchCriteria criteria) {
        this.criteria = criteria;
    }

    @Override
    public Predicate toPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Expression<String> expression = root.get(criteria.getKey());

        if (criteria.isHasInnerField()) {
            final Class<?> clazz = root.getJavaType();
            Optional<Field> keyWithInnerField = Arrays //tu mam pole address
                    .stream(clazz.getDeclaredFields())
                    .filter(field -> field.getName().equals(criteria.getKey()))
                    .findFirst();
            if (keyWithInnerField.isPresent()) {
                Class<?> innerFieldClass = keyWithInnerField.get().getType();
                if (clazz.isAnnotationPresent(Entity.class) && innerFieldClass.isAnnotationPresent(Entity.class)) {

                    if (clazz.isAssignableFrom(Restaurant.class)) {
                        Join<Restaurant, Address> join = root.join(criteria.getKey());
                        expression = join.get(criteria.getInnerField());
                    } else if (clazz.isAssignableFrom(User.class)) {
                        Join<User, Address> join = root.join(criteria.getKey());
                        expression = join.get(criteria.getInnerField());
                    }
                }
            }
        }

        switch (criteria.getOperation()) {
            case EQUALITY:
                return builder.equal(expression, criteria.getValue());
            case NEGATION:
                return builder.notEqual(expression, criteria.getValue());
            case GREATER_THAN:
                return builder.greaterThan(expression, criteria.getValue().toString());
            case LESS_THAN:
                return builder.lessThan(expression, criteria.getValue().toString());
            case LIKE:
                return builder.like(expression, criteria.getValue().toString());
            case STARTS_WITH:
                return builder.like(expression, criteria.getValue() + "%");
            case ENDS_WITH:
                return builder.like(expression, "%" + criteria.getValue());
            case CONTAINS:
                return builder.like(expression, "%" + criteria.getValue() + "%");
            default:
                return null;
        }
    }

    public SearchCriteria getCriteria() {
        return criteria;
    }
}

package pl.kubson_eats.crud.exception;

public class LastPositionException extends RuntimeException {
    public LastPositionException() {
        super("Extreme position");
    }
}

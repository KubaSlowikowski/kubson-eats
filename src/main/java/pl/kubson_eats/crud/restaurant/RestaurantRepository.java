package pl.kubson_eats.crud.restaurant;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;
import pl.kubson_eats.crud.abstraction.CommonRepository;
import th.co.geniustree.springdata.jpa.repository.JpaSpecificationExecutorWithProjection;

import java.util.Optional;

@Repository
public interface RestaurantRepository extends CommonRepository<Restaurant>, JpaSpecificationExecutorWithProjection<Restaurant> {

    Optional<Restaurant> findByName(String name);

    @Override
    @EntityGraph(attributePaths = {"products", "address"})
    Page<Restaurant> findAll(Specification<Restaurant> specification, Pageable pageable);
}

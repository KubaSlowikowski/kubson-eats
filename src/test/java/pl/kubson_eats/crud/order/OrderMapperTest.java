package pl.kubson_eats.crud.order;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import pl.kubson_eats.crud.product.ProductMapper;
import pl.kubson_eats.utils.Utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@RunWith(MockitoJUnitRunner.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class OrderMapperTest {

    @InjectMocks
    private final OrderMapper orderMapper = OrderMapperImpl.INSTANCE;

    @BeforeAll
    void init() {
        ProductMapper productMapper = Mappers.getMapper(ProductMapper.class);
        ReflectionTestUtils.setField(orderMapper, "productMapper", productMapper);
    }

    @Test
    void should_map_order_to_orderDto() {
        //given
        Order order = Utils.getOrder();
        OrderDTO orderDTO = Utils.getOrderDTO();

        //when
        OrderDTO result = orderMapper.toDto(order);

        //then
        assertEquals(orderDTO, result);
    }

    @Test
    void shouldnt_map_order_to_orderDto() {
        //given
        Order order = Utils.getOrder();
        OrderDTO orderDTO = Utils.getOrderDTO();
        orderDTO.setUserId(1000L);

        //when
        OrderDTO result = orderMapper.toDto(order);

        //then
        assertNotEquals(orderDTO, result);
    }

    @Test
    void should_map_orderDto_to_order() {
        //given
        OrderDTO orderDTO = Utils.getOrderDTO();
        Order order = Utils.getOrder();
        order.setCreatedOn(null);

        //when
        Order result = orderMapper.fromDto(orderDTO);

        //then
        assertEquals(order, result);
    }

    @Test
    void shouldnt_map_orderDto_to_order() {
        //given
        OrderDTO orderDTO = Utils.getOrderDTO();
        Order order = Utils.getOrder();
        order.setCreatedOn(null);
        order.setOrderComment("adsjhwq342");

        //when
        Order result = orderMapper.fromDto(orderDTO);

        //then
        assertNotEquals(order, result);
    }
}

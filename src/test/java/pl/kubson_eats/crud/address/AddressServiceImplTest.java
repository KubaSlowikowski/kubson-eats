package pl.kubson_eats.crud.address;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import pl.kubson_eats.crud.abstraction.AbstractService;
import pl.kubson_eats.crud.abstraction.AbstractServiceTest;
import pl.kubson_eats.crud.abstraction.CommonMapper;
import pl.kubson_eats.crud.abstraction.CommonRepository;

import static org.junit.jupiter.api.Assertions.*;

public class AddressServiceImplTest extends AbstractServiceTest<Address, AddressDTO> {

    @InjectMocks
    private AddressServiceImpl addressService;
    @Mock
    private AddressRepository addressRepository;
    @Mock
    private AddressMapper addressMapper;


    @Override
    public AbstractService<Address, AddressDTO> getService() {
        return addressService;
    }

    @Override
    public CommonRepository<Address> getRepostiory() {
        return addressRepository;
    }

    @Override
    public CommonMapper<Address, AddressDTO> getMapper() {
        return addressMapper;
    }

    @Override
    public Class<AddressDTO> getDtoClass() {
        return AddressDTO.class;
    }

    @Override
    public Class<Address> getEntityClass() {
        return Address.class;
    }
}
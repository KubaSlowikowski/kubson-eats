package pl.kubson_eats.crud.address;

import org.springframework.stereotype.Service;
import pl.kubson_eats.crud.abstraction.AbstractService;

@Service
public class AddressServiceImpl extends AbstractService<Address, AddressDTO> implements AddressService {

    protected AddressServiceImpl(final AddressMapper addressMapper, final AddressRepository addressRepository) {
        super(addressMapper, addressRepository);
    }
}

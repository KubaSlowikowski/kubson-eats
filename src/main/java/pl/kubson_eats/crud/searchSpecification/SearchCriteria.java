package pl.kubson_eats.crud.searchSpecification;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchCriteria {
    private String key;
    private boolean hasInnerField;
    private String innerField;
    private SearchOperation operation;
    private Object value;
    private boolean orPredicate;

    public SearchCriteria(final String key, final String innerField, final SearchOperation operation, final Object value, final String orPredicate) {
        this.orPredicate
                = orPredicate != null
                && orPredicate.equals(SearchOperation.OR_PREDICATE_FLAG);
        this.key = key;
        if (innerField != null && innerField.startsWith(".")) {
            this.hasInnerField = true;
            this.innerField = innerField.substring(1, innerField.length());
        }
        this.operation = operation;
        this.value = value;
    }
}

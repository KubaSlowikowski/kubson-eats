package pl.kubson_eats.crud.product;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kubson_eats.crud.abstraction.AbstractEntity;
import pl.kubson_eats.crud.image.Image;
import pl.kubson_eats.crud.restaurant.Restaurant;
import pl.kubson_eats.utils.ProductCategory;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "products")
@NoArgsConstructor
@Getter
@Setter
public class Product extends AbstractEntity {

    private String name;

    private BigDecimal price;

    private String description;

    private Integer position;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_id")
    private Restaurant restaurant;

    @Enumerated(EnumType.STRING)
    private ProductCategory category;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "image_id", referencedColumnName = "id")
    private Image image;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return Objects.equals(getId(), product.getId()) &&
                Objects.equals(name, product.name) &&
                Objects.equals(price, product.price) &&
                Objects.equals(description, product.description) &&
                Objects.equals(position, product.position) &&
                Objects.equals(restaurant, product.restaurant) &&
                Objects.equals(category, product.category) &&
                Objects.equals(image, product.image) &&
                Objects.equals(getCreatedOn(), product.getCreatedOn());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public static final class ProductBuilder {
        private Long id;
        private LocalDateTime createdOn;
        private String name;
        private BigDecimal price;
        private String description;
        private Integer position;
        private Restaurant restaurant;
        private ProductCategory category;
        private Image image;

        private ProductBuilder() {
        }

        public static ProductBuilder aProduct() {
            return new ProductBuilder();
        }

        public ProductBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public ProductBuilder withCreatedOn(LocalDateTime createdOn) {
            this.createdOn = createdOn;
            return this;
        }

        public ProductBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public ProductBuilder withPrice(BigDecimal price) {
            this.price = price;
            return this;
        }

        public ProductBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public ProductBuilder withPosition(Integer position) {
            this.position = position;
            return this;
        }

        public ProductBuilder withRestaurant(Restaurant restaurant) {
            this.restaurant = restaurant;
            return this;
        }

        public ProductBuilder withCategory(ProductCategory category) {
            this.category = category;
            return this;
        }

        public ProductBuilder withImage(Image image) {
            this.image = image;
            return this;
        }

        public Product build() {
            Product product = new Product();
            product.setId(id);
            product.setCreatedOn(createdOn);
            product.setName(name);
            product.setPrice(price);
            product.setDescription(description);
            product.setPosition(position);
            product.setRestaurant(restaurant);
            product.setCategory(category);
            product.setImage(image);
            return product;
        }
    }
}

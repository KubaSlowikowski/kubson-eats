package pl.kubson_eats.crud.user;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.kubson_eats.crud.abstraction.AbstractController;

@RestController
@RequestMapping("/api/users")
public class UserController extends AbstractController<UserService, UserDTO> {

    protected UserController(final UserService userService) {
        super(userService);
    }
}
